# Skeleton React JS + Firebase + Jumbo React

Skeleton creado para desarrollar aplicaciones basadas en React Js y Firebase.
Usa el template [Jumbo React] (http://docs.g-axon.work/jumbo-react/) y se conecta con Firestore.

## Pasos previos

Se debe tener instalados en el PC o Mac:

- **Node Js** http://nodejs.org/
- **Yarn** https://classic.yarnpkg.com/en/docs/install/#mac-stable
- **Ruby** https://www.ruby-lang.org
- **Git** https://gist.github.com/derhuerst/1b15ff4652a867391f03

## Entornos

El skeleton cuenta con 3 entornos:

- **Local** es usado para desarrollo. La url es http://localhost:3000/.
- **Test** es usado para probar el desarrollo antes de hacer el deploy en producción. El deploy a este proyecto se debe ejecutar en el directorio local de tus proyectos. Este entorno se debe trabajar con la rama **dev** del repositorio.
- **Production** entorno de producción. Este entorno debe trabajar con la rama **master** del repositorio.

# Set Up

1) Clonar la rama **dev** del repositorio en la carpeta donde trabajes los proyectos (debes tener accesos)

```bash
git clone -b dev <url repositorio>
```

2) Ingresar a la carpeta skeleton-jumbo-react e instalar las herramientas de desarrollo de Firebase

```bash
yarn add firebase --dev
```

3) Ejecutar el comando **firebase init** para conectar la aplicación con el proyecto de firebase (importante no sobre-escribir nada).

```bash
firebase init
```

4) Ejecutar **yarn start** para iniciar la aplicación

```bash
yarn start
```
La url para acceder localmente a la aplicación es http://localhost:3000/

## Despliegues

Cuando se vaya a realizar un despliegue los pasos a seguir son:

1) Ejecutar **npm install -g firebase-tools** si no se tienen las herramientas CLI de Firebase (se instala global)

```bash
npm install -g firebase-tools
```

2) Entrar por terminal al proyecto **app-dev-cemtrik** (entorno de pruebas).

```bash
cd projects/<directorio del proyecto>
```

3) Ejecutar el comando **yarn build** para construir la aplicación de pruebas (test).

```bash
yarn build
```

4) Ejecutar el comando **firebase deploy** para desplegar la aplicación en el entorno de pruebas (test).

```bash
firebase deploy
```

## Aspectos Importantes

En firestore se deben crear los indices compuestos para las consultas de varios registros con filtros (where).

Las reglas del firestore de deben revisar para que queden seguras (verificar que solo se pueda acceder si el usuario está logueado).

Se debe habilitar en firebase > autenticación el método email / password y crear un usuario para poder loguearse en el app.

El app se encuentra en el directorio **src**

El directorio de producción es **build**. Cuando se ejecuta yarn build todo se construye minificado en este directorio.

El archivo de configuración de Firebase está en **src/firebase/firebase.js**. El app usa **Firestore** para la base de datos. Este archivo usa unas constantes para exportar funcionalidades de firebase como auth, firestore, googleAuthProvider, etc.

En el directorio src/config/ se encuentran los archivos con las variables de configuración de acuerdo al entorno: dev.js para local y test y prod.js para producción.

En la ruta src/actions/Helpers.js se encuentran los métodos globales del app.

En el directorio src/containers/SideNav se encuentra el archivo **SidenavContent.js** donde se agregan las opciones del menú lateral izquierdo (sidebar). El Menú tiene un contenedor principal y contenedores hijos: ejemplo **equipo** es el contenedor principal con los contenedores hijos **roles**, **personas** etc.

El contenedor principal debe tener su ruta configurada en el archivo src/app/index.js

Cada contenedor principal debe estar creado con su respectiva carpeta en el directorio src/app/routes/<contenedor_principal>. Ejemplo para equipo: src/app/routes/equipo. En este directorio va toda la configuración del contenedor (componentes).

EL contenedor principal tiene un archivo **index.js** donde va la configuración de las rutas de sus contenedores hijos. Además tiene un directorio **routes** donde van los contenedores hijos con sus respectivas carpetas.

Cada contenedor hijo (componente) tiene un archivo **index.js** donde están todas sus funciones. La estructura de un componente es:

- Importación de paquetes
- **constructor()** método para crear los estados del componentes
- **componentDidMount()** para obtener los registros de la colección del componente (colección de firestore) y setear los estados
- **showAlert(type, message)** para los mensajes después de cada acción: crear, editar, borrar etc.
- **resetForm()** para resetear los datos del formulario y el valor de los estados.
- **updateInput** para habilitar el seteo el estado de cada campo en el formulario.
- **action<NombreComponente)** encargado de determinar si agrega o actualiza un registro a la colección. Este método es invocado en la etiqueta <form>
- **get<NombreComponente>** encargado de obtener un registro de la colección (se le pasa el id).
- **add** es encargado de agregar registros a la colección.
- **update** es encargado de actualizar registros (recibe el id del registro)
- **delete<NombreComponente>** encargado de borrar registros de la colección.
- **validateForm** encargado de validar el formulario.

Después de ejecutar cada acción se deben setear los estados del componente a vacíos.

Es muy importante tener en cuenta para cada componente:

- Los estados de cada componente son: id, edit (true o false), un estado para cada campo de la colección, un estado para el conjunto completo del componente (ejemplo roles : []), alert (true o false para mostrar mensajes) y alertData que recibe el tipo de mensaje (success, warning o danger) y el texto del mensaje.

- El conjunto de registros de la colección se deben usar en el estado del componente para ese fin. Ejemplo para roles el estado para el conjunto de datos debe ser **roles: []** y su estructura es: id (id del documento en firestore) y data (objeto que tiene cada campo de la colección).

- Si el package.json es actualizado en un pull (git pull origin dev) se debe ejecutar primero yarn para actualizar o instalar los paquetes (node_modules). Después se ejecuta yarn start para iniciar el app en modo desarrollo.

- El pull y push se deben hacer sobre la rama **dev** mientras se esté desarrollando. Cuando una funcionalidad o grupo de funcionalidades están probadas en el entorno **test** se hace un merge a la rama **master**.

## Comandos GIT para tener en cuenta

- **git clone -b <rama>** <link rama> para clonar una rama especifica.
- **git branch** para saber en que rama se está trabajando.
- **git fetch && git checkout <nombre rama>** para cambiar de rama.
- **git add .** para agregar todos los archivos al staging.
- **git commit -m "mensaje commit"** para hacer el commit.
- **git push origin <nombre rama>** para el push en una rama especifica.
- **git pull origin <nombre rama>** para el pull de una rama especifica.
- **git merge <rama>** para hacer merge de la rama actual con la rama especifica.
