import firebase from 'firebase';
//se valida si la app está corriendo en modo pruebas o producción (se cambia app.cemtrik.com por el dominio de producción)
const configuracion = window.location.hostname === "app.cemtrik.com" ? require('../config/prod') : require('../config/dev');
// Config app cemtrik
const config = {
  apiKey: "AIzaSyD7ywgrRVFq1J_2Zl_ICAV66R8H9beyvhQ",
  authDomain: "datea-3b516.firebaseapp.com",
  databaseURL: "https://datea-3b516.firebaseio.com",
  projectId: "datea-3b516",
  storageBucket: "datea-3b516.appspot.com"
};
//Inicio de aplicación App cemtrik
const app_cemtrik = firebase.initializeApp(config, configuracion.projectId_app);
const auth = app_cemtrik.auth();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();
const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();
const firestore = app_cemtrik.firestore();
//firestore.settings({timestampsInSnapshots:true});

export {
  auth,
  googleAuthProvider,
  githubAuthProvider,
  facebookAuthProvider,
  twitterAuthProvider,
  firestore,
  firebase
};

export default firebase;
