import React from 'react';
import {firestore,firebase} from '../../../../../firebase/firebase';
import ContainerHeader from 'components/ContainerHeader/index';
import CardBox from 'components/CardBox/index';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import Inputs from '../../../components/routes/textFields/inputs/Inputs';
import IntlMessages from 'util/IntlMessages';
import MaterialTable from 'material-table'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import {validateForms, newAttributes, setDateFormat} from 'actions/Helpers';

//const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const generos = [
  {
    value: 'Masculino',
    label: 'Masculino',
  },
  {
    value: 'Femenino',
    label: 'Femenino',
  }
];

class Personas extends React.Component {
  //estados del componente
  constructor(props) {
    super(props);
    this.state = {
      personas: [],
      nombres : '',
      apellidos : '',
      telefono : '',
      correo : '',
      sexo: '',
      edit : false,
      id : '',
      alert: false,
      alertData: {},
      open: false,
      open_roles: false,
      rules:[
        {
            field: "nombres",
            label: "Nombres",
            type: "text",
            required: true
        },
        {
            field: "apellidos",
            label: "Apellidos",
            type: "text",
            required: true
        },
        {
            field: "telefono",
            label: "Teléfono",
            type: "text",
            required: true
        },
        {
            field: "correo",
            label: "Correo",
            type: "text",
            required: true
        },
        {
            field: "sexo",
            label: "Género",
            type: "select",
            required: true
        }
      ],
      errors: {
        nombres: '',
        apellidos: '',
        telefono: '',
        correo: '',
        sexo: ''
      }
    };
  }

  componentDidMount() {
    //se traen los documentos de la colección personas
    firestore.collection('personas').where('estado_eliminado', '==', false).orderBy("nombres").onSnapshot((snapShots) => {
      this.setState({
        personas: snapShots.docs.map((doc) => {
          return {id:doc.id, data: doc.data(), nombres: doc.data().nombres, apellidos: doc.data().apellidos, telefono: doc.data().telefono, correo: doc.data().correo, sexo: doc.data().sexo};
        })
      })
    }, error => {
      console.log(error);
    });

      //El id del usuario logueado se obtiene con auth.W
      //console.log(auth.W);
  }

  showAlert(type, message) {
    this.setState({
      alert: true,
      alertData: { type, message }
    });
    setTimeout(() => {
      this.setState({ alert: false });
    }, 4000)
  }

  resetForm() {
    this.refs.personasForm.reset();
    //después de agregar o editar se setean los campos nombres
    this.setState({
      nombres: "",
      apellidos: "",
      telefono: "",
      correo : "",
      sexo: "",
      id: "",
      edit :false,
      open: false,
      errors: {
        nombres: '',
        apellidos: '',
        telefono: '',
        correo: '',
        sexo: ''
      }
    });
  }

  updateInput = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    //console.log(this.state.roles);
  }

  validateForm = () => {
  //se llama la función global para validar formularios
  let results = validateForms(this.state);
  //la función global validateForms() retorna los errores y si el formulario pasa la validación
  this.setState({
    errors: results[0],
    //validForm: results[1]
  });
  //console.log(results[1]);
  return results[1];
}

  actionDocumento = e => {
    e.preventDefault();
    //JULIAN: si el formulario pasa la validación
    if(this.validateForm()) {
      if(!this.state.edit) {
        this.add();
      } else {
        this.update();
      }
    }
  }

  getDocumento = (id) => {
    let docRef = firestore.collection('personas').doc(id);

    docRef.get().then((doc) => {
      if(doc.exists) {
        this.setState({
          nombres: doc.data().nombres,
          apellidos : doc.data().apellidos,
          telefono : doc.data().telefono,
          correo : doc.data().correo,
          sexo : doc.data().sexo,
          roles: doc.data().roles,
          edit: true,
          id: doc.id,
          open: true
        })
      } else {
        console.log("El documento no existe");
      }
    }).catch((error) => {
      console.log(error);
    })
  }

  add = () => {
    const userRef = firestore.collection('personas').add({
      nombres: this.state.nombres,
      apellidos: this.state.apellidos,
      telefono: this.state.telefono,
      correo: this.state.correo,
      sexo: this.state.sexo,
      estado_registro: true,
      estado_eliminado: false,
      creado_por: newAttributes().correo,
      fecha_creacion: setDateFormat(new Date()),
      modificado_por: null,
      fecha_eliminacion: null,
      fecha_modificacion: null,
      eliminado_por: null,
      nombre_creadoPor: newAttributes().correo,
      nombre_eliminadoPor: null,
      nombre_modificadoPor: null
    }).then(() => {
      this.showAlert('success', 'La persona ha sido creada con éxito');
    }).catch(() => {
      this.showAlert('danger', 'La persona NO fue creada correctamente');
    });
    this.resetForm();

  }

  update = () => {
    const {id, nombres, apellidos, telefono, correo, sexo} = this.state;
    firestore.collection('personas').doc(id).update({
      nombres: nombres,
      apellidos: apellidos,
      telefono: telefono,
      correo: correo,
      sexo: sexo,
      modificado_por: newAttributes().correo,
      fecha_modificacion: setDateFormat(new Date()),
      nombre_modificadoPor: newAttributes().correo
    }).then( () => {
      this.showAlert('success', 'La persona se ha actualizado con éxito');
    }).catch((error) => {
      this.showAlert('danger', 'La persona NO ha sido actualizada correctamente');
    });
    this.resetForm();
  }

  delete = (id) => {
    firestore.collection('personas').doc(id).update({
      estado_eliminado: true,
      eliminado_por: newAttributes().correo,
      fecha_eliminacion: setDateFormat(new Date()),
      nombre_eliminadoPor: newAttributes().correo
    }).then( () => {
      this.showAlert('success', 'La persona se ha eliminado con éxito');
    }).catch((error) => {
      this.showAlert('danger', 'La persona NO se ha eliminado correctamente');
    });
  }

  handleClickOpen = () => {
    //Cuando se hace click en el botón click de una persona se abre la modal de roles
    //se setean los checks de todos los roles del estado roles_list a false
    if(!this.state.open) {
      //se setea el estado open a true para que abra la modal
      this.setState({open: true});
    }

  };

  handleRequestClose = () => {
    this.resetForm();
    this.setState({open: false});
  };

  render() {

    const {errors} = this.state;
    return (
      <div className="animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={this.props.match} title={
          <IntlMessages id="personas"/>}/>
          {this.state.alert && <div className={`alert alert-${this.state.alertData.type}`} role='alert'>
            <div className='container'>
              {this.state.alertData.message}
            </div>
          </div>}

          <div className='row'>
          <Dialog open={this.state.open} onClose={this.handleRequestClose}>
            <DialogTitle>{this.state.edit ? 'Editar' : 'Agregar'} persona</DialogTitle>
            <DialogContent>
            <form onSubmit={this.actionDocumento} ref='personasForm' >
            <FormControl className="w-100 mb-2" error={errors.nombres.length > 0 ? true: false}>
              <TextField
                error={errors.nombres.length > 0 ? true: false}
                name="nombres"
                label="Nombres"
                value={this.state.nombres}
                onChange={this.updateInput}
                onBlur={this.validateForm}
                margin="normal"
                fullWidth
                noValidate
              />
              {errors.nombres.length > 0 &&
                <FormHelperText>{errors.nombres}</FormHelperText>}
            </FormControl>
            <FormControl className="w-100 mb-2" error={errors.apellidos.length > 0 ? true: false}>
              <TextField
                error={errors.nombres.length > 0 ? true: false}
                name="apellidos"
                label="Apellidos"
                value={this.state.apellidos}
                onChange={this.updateInput}
                onBlur={this.validateForm}
                margin="normal"
                fullWidth
              />
              {errors.apellidos.length > 0 &&
                <FormHelperText>{errors.apellidos}</FormHelperText>}
            </FormControl>
            <FormControl className="w-100 mb-2" error={errors.telefono.length > 0 ? true: false}>
                <TextField
                  error={errors.telefono.length > 0 ? true: false}
                  name="telefono"
                  label="Teléfono"
                  value={this.state.telefono}
                  onChange={this.updateInput}
                  onBlur={this.validateForm}
                  margin="normal"
                  fullWidth
                />
              {errors.telefono.length > 0 &&
                <FormHelperText>{errors.telefono}</FormHelperText>}
            </FormControl>
            <FormControl className="w-100 mb-2" error={errors.correo.length > 0 ? true: false}>
                <TextField
                  error={errors.correo.length > 0 ? true: false}
                  name="correo"
                  label="Correo"
                  value={this.state.correo}
                  onChange={this.updateInput}
                  onBlur={this.validateForm}
                  margin="normal"
                  fullWidth
                />
                {errors.correo.length > 0 &&
                  <FormHelperText>{errors.correo}</FormHelperText>}
              </FormControl>
              <FormControl className="w-100 mb-2" error={errors.sexo.length > 0 ? true: false}>
                <TextField
                  error={errors.sexo.length > 0 ? true: false}
                  name ="sexo"
                  select
                  label="Género"
                  value={this.state.sexo}
                  onChange={this.updateInput}
                  onBlur={this.validateForm}
                  SelectProps={{}}
                  helperText="Por favor seleccione el género"
                  margin="normal"
                  fullWidth
                >
                  {generos.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                {errors.sexo.length > 0 &&
                      <FormHelperText>{errors.sexo}</FormHelperText>}
              </FormControl>

              <div className='text-center'>
              <br/>
                <button type='submit' className='btn btn-primary'>
                {this.state.edit ? 'Editar' : 'Agregar'}
                </button>
                <button type='reset' className='btn btn_r_color div_return_btn' onClick={() => this.resetForm()}>
                Reiniciar
                </button>
              </div>
            </form>
            </DialogContent>
          </Dialog>
          <CardBox styleName="col-lg-12"
                   heading={<IntlMessages id="Listado de personas"/>}>
                   <div className="table-responsive-material">
                   <Button onClick={this.handleClickOpen} className="rounded-button  bg-primary" color="primary">
                     <i className="material-icons rounded-button-color">add</i>
                   </Button>
                   <br/>
                   <br/>
                   <MaterialTable
                       title=""
                       localization={{
                           pagination: {
                               labelDisplayedRows: '{from}-{to} de {count}',
                               labelRowsSelect: 'Registros',
                               firstTooltip: 'Inicio',
                               previousTooltip: 'Anterior',
                               nextTooltip: 'Siguiente',
                               lastTooltip: 'Fin'
                           },
                           toolbar: {
                               nRowsSelected: '{0} filas selecionadas',
                               searchPlaceholder: 'Buscar...'
                           },
                           header: {
                               actions: 'Acciones'
                           },
                           body: {
                               emptyDataSourceMessage: 'No hay registros para mostrar',
                               filterRow: {
                                   filterTooltip: 'Filtrar'
                               },
                               deleteTooltip: 'Borrar',
                               editRow:{
                                 deleteText: '¿Estás seguro que deseas borrar el registro?',
                                 cancelTooltip: 'Cancelar',
                                 saveTooltip: 'Confirmar'
                               }
                           }
                       }}
                       columns={[
                         {title: 'Nombres', field: 'nombres'},
                         {title: 'Apellidos', field: 'apellidos' },
                         {title: 'Teléfono', field: 'telefono' },
                         {title: 'Correo', field: 'correo' },
                         {title: 'Sexo', field: 'sexo' }
                       ]}
                       data={this.state.personas}
                       actions={[
                       {
                         icon: 'edit',
                         tooltip: 'Editar',
                         onClick: (event, rowData) => {
                           this.getDocumento(rowData.id)
                         }
                       },

                     ]}
                     editable={{
                       onRowDelete: oldData =>
                         new Promise((resolve, reject) => {
                           setTimeout(() => {
                             {
                               this.delete(oldData["id"])
                               //console.log(oldData["id"]);
                               //let data = this.state.data;
                               //const index = data.indexOf(oldData);
                               //data.splice(index, 1);
                               //this.setState({ data }, () => resolve());
                             }
                             resolve()
                           }, 1000)
                         }),
                     }}
                     />
                   </div>
            </CardBox>
          </div>
      </div>
    );
  }
}

export default Personas;
