const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const fetch   = require('node-fetch');

const app = express();
app.use(cors({ origin: true }));

//const db = admin.firestore();

exports.app = functions.https.onRequest(app);
